﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DanceStudio
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Dance timer that gets controlled by Start / Stop dance
        /// </summary>
        private DispatcherTimer _tmDanceTimer;

        public MainPage()
        {
            this.InitializeComponent();

            //create and initialize the timer
            _tmDanceTimer = new DispatcherTimer();
            _tmDanceTimer.Interval = TimeSpan.FromMilliseconds(250);
            _tmDanceTimer.Tick += OnDanceShuffle;
        }


        private void OnDanceStep(object sender, RoutedEventArgs e)
        {
            const int DANCE_STEP_SIZE = 10;
            if (sender == _btnLeft)
            {
                //determine the new position of the image
                double newLeft = Canvas.GetLeft(_imgAvatar) - DANCE_STEP_SIZE;

                //move the image avatar 10 pixels to the left
                Canvas.SetLeft(_imgAvatar, newLeft);
            }
            else if (sender == _btnUp)
            {
                //determine the new position of the image
                double newTop = Canvas.GetTop(_imgAvatar) - DANCE_STEP_SIZE;

                //move the image avatar 10 pixles up
                Canvas.SetTop(_imgAvatar, newTop);

            }
            else if (sender == _btnDown)
            {
                //move the image avatar 10 pixels down
                //determine the new position of the image
                double newTop = Canvas.GetTop(_imgAvatar) + DANCE_STEP_SIZE;

                //move the image avatar 10 pixles up
                Canvas.SetTop(_imgAvatar, newTop);
            }
            else if (sender == _btnRight)
            {
                //move the image avatar 10 pixels to the right
                //determine the new position of the image
                double newLeft = Canvas.GetLeft(_imgAvatar) + DANCE_STEP_SIZE;

                //move the image avatar 10 pixels to the left
                Canvas.SetLeft(_imgAvatar, newLeft);
            }
            else if (sender == _btnRotateLeft)
            {
                //rotate the image avatar by 10 degrees left
                _imgTransform.Rotation -= DANCE_STEP_SIZE;
            }
            else if (sender == _btnRotateRight)
            {
                //rotate the image vatar by 10 degrees right
                _imgTransform.Rotation += DANCE_STEP_SIZE;
            }
            else
            {
                //Defensive programming using asserts
                Debug.Assert(false, "Unknown button for dance step. Button click will be ignored.");
            }
        }

        private void OnStartDance(object sender, RoutedEventArgs e)
        {
            if (_tmDanceTimer.IsEnabled)
            {
                //stop the timer
                _tmDanceTimer.Stop();

                //update the button caption
                _btnStartDance.Content = "Start Dance";
            }
            else
            {
                //start the dance timer
                _tmDanceTimer.Start();

                //update the button caption for next action
                _btnStartDance.Content = "Stop Dance";
            }

        }

        /// <summary>
        /// This event is triggered by the timer, every second or whatever the timer.Interval is set to
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDanceShuffle(object sender, object e)
        {
            //change the scale factor to make the image flip horizontally
            _imgTransform.ScaleX *= -1;
        }
    }
}
